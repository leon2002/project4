#pragma once
#include <iostream>
#include "OutStream.h"


class FileStream : public OutStream{
public:
	FileStream(const char* str);
	~FileStream();

	FileStream& operator<<(const char* str);
	FileStream& operator<<(int num);
};

void endline();