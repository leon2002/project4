#include "OutStreamEncrypted.h"
#include "OutStream.h"
#include <stdio.h>

OutStreamEncrypted::OutStreamEncrypted(int _offset)
{
	this->_offset = _offset;
}

OutStream& OutStreamEncrypted::operator<<(char* str)
{
	int i = 0;
	for (i = 0; i < strlen(str); i++) {
		if (int(str[i]) >= START) {
			if (int(str[i]) + this->_offset <= END)
				str[i] = char(int(str[i]) + this->_offset);
			else
				str[i] = char(((int(str[i]) + this->_offset) % END) + START - 1);
		}
	}
	return OutStreamEncrypted::OutStream::operator<<(str);


}
