#include "OutStream.h"
#include "OutStreamEncrypted.h"
#include "FileStream.h"
#include "Logger.h"
#include <iostream>

int main(int argc, char** argv)
{
	// part 1
	char _first[] = "I am the Doctor and I'm ";
	int _age = 1500;
	char _last[] = " years old";

	OutStream OS;
	OS << (_first);
	OS << (_age);
	OS << (_last);
	OS << (endline);
	OS.~OutStream();


	// part 2
	char _fName[] = "file.txt";
	
	FileStream FS(_fName);
	FS << (_first);
	FS << (_age);
	FS << (_last);
	FS.~FileStream();
	printf("\n");


	// part 3

	int _offset = 1;
	char _str[] = "ab~c";

	OutStreamEncrypted OSE(_offset);
	OSE << (_str);
	printf("\n");



	// part 4

	char _f[] = "Logger.txt";
	bool _logToScreen = true;
	Logger _log(_f, _logToScreen);
	_log.print("My Name is Leon yoyoyo...\n");
	_log.print("I love cookies.\n");
	_log.print("and more..\n");
	_log.print("and more...\n");
	
	




	system("PAUSE");
	return 0;
}
