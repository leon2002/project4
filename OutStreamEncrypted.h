#pragma once
#include "OutStream.h"
#include <string.h>

#define START 32
#define END 126


class OutStreamEncrypted : public OutStream {
private:
	int _offset;

public:
	OutStreamEncrypted(int _offset);

	OutStream& operator<<(char* str);


};