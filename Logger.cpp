#include "Logger.h"
#include "OutStream.h"
#include "FileStream.h"
#include <stdio.h>
#include <iostream>

Logger::Logger(const char* filename, bool logToScreen) : 
	fs(FileStream(filename)), os(OutStream())
{
	this->_flag = logToScreen;
	this->var = 1;

}

Logger::~Logger()
{
}

void Logger::print(const char* msg)
{
	this->fs.operator<<(int(var));
	this->fs.operator<<(msg);
	if (this->_flag) {
		this->os.operator<<(int(var));
		this->os.operator<<(msg);
	}
	this->var++;
}
