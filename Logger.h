#pragma once

#include "OutStream.h"
#include "FileStream.h"
#include <iostream>

class Logger
{
private:
	OutStream os;
	FileStream fs;
	bool _flag;

	unsigned int var;

public:
	Logger(const char* filename, bool logToScreen);
	~Logger();

	void print(const char* msg);
};
