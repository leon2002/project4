#pragma once
#include <iostream>
#include <stdio.h>


class OutStream {
protected:
	FILE* f = stdout;
public:
	OutStream();
	~OutStream();

	OutStream& operator<<(const char* str);
	OutStream& operator<<(int num);
	OutStream& operator<<(void(*pf)());
};

void endline();
